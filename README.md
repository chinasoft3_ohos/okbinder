# okbinder

## 项目介绍
- 项目名称: okbinder
- 所属系列: openharmony的第三方组件适配移植
- 功能:是一个轻量级的跨进程通信方案，可以用来替代 AIDL。
- 项目移植状态: 主功能完成
- 调用差异:无
- 开发版本:sdk6,DevEco Studio 2.2 Beta1
- 基线版本:Release 2.0

## 效果演示

![输入图片说明](https://gitee.com/chinasoft3_ohos/okbinder/raw/master/img/okbiner.gif "okbiner.gif")

## 安装教程

1.在项目根目录下的build.gradle文件中

```

allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}

```
2.在entry模块的build.gradle文件中，
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:okbinder2:1.0.0')
    ......  
 }
 
 ```
 在sdk6,DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件, 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
```
1、使用该库很简单，只需要查看sample即可

2、config中新增服务配置，并Intent启动服务
{
  "name": "cn.thens.okbinder2.sample.RemoteService",
  "icon": "$media:icon",
  "description": "$string:HelloWorld",
  "type": "service",
  "process": ":f1"
}

3、客户端绑定
public class LocalService extends Ability {
    @Override
    protected IRemoteObject onConnect(Intent intent) {
        return OkBinder.create(new IRemoteServiceImpl("LocalService"));
    }
}

4、客户端跨进程请求
MainAbilitySlice.this.connectAbility(intents, new IAbilityConnection() {
                @Override
                public void onAbilityConnectDone(ElementName elementName, IRemoteObject remote, int i) {
                    IRemoteService proxy = OkBinder.proxy(IRemoteService.class, remote);
                    proxy.testError(false, replace);

                @Override
                public void onAbilityDisconnectDone(ElementName elementName, int i) {
                }
            });
```
## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代

- 1.0.0

## 版权和许可信息
- MIT

