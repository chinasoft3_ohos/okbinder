package cn.thens.okbinder2;

import com.squareup.javapoet.*;

import javax.annotation.processing.Filer;
import javax.lang.model.element.*;
import javax.lang.model.util.Elements;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class FactoryGenerator {
    private static final boolean IS_SHOULD_SKIP_OBJECT_METHODS = true;
    private final RelatedTypes t;
    private final Elements elementUtils;
    private final Filer filer;

    /**
     * 构造
     *
     * @param types 类型
     * @param elementUtils element
     * @param filer filer
     */
    public FactoryGenerator(RelatedTypes types, Elements elementUtils, Filer filer) {
        this.t = types;
        this.elementUtils = elementUtils;
        this.filer = filer;
    }

    public void generate(TypeElement element) {
        TypeName MyInterface = ClassName.get(element.asType());
        String packageName = elementUtils.getPackageOf(element).getQualifiedName().toString();

        List<FieldSpec> factoryFields = new ArrayList<>();
        List<MethodSpec> proxyMethods = new ArrayList<>();
        CodeBlock.Builder binderFunctionCode = CodeBlock.builder();
        int methodCount = 0;
        for (Element member : elementUtils.getAllMembers(element)) {
            if (shouldSkipMethod(member)) {
                continue;
            }
            ExecutableElement methodMember = (ExecutableElement) member;
            String methodName = methodMember.getSimpleName().toString();
            TypeName returnType = ClassName.get(methodMember.getReturnType());
            String functionIdName = methodName + "_" + methodCount;

            List<CodeBlock> functionInvokeArgs = new ArrayList<>();
            List<ParameterSpec> proxyMethodParams = new ArrayList<>();
            CodeBlock.Builder proxyMethodInvokeArgCode = CodeBlock.builder();
            for (VariableElement parameter : methodMember.getParameters()) {
                TypeName ParamType = ClassName.get(parameter.asType());
                int index = functionInvokeArgs.size();
                functionInvokeArgs.add(CodeBlock.of("($T) args[$L]", ParamType, index));
                proxyMethodParams.add(ParameterSpec.builder(ParamType, "arg" + index)
                        .build());
                if (index == 0) {
                    proxyMethodInvokeArgCode.add(", (Object) arg" + index);
                } else {
                    proxyMethodInvokeArgCode.add(", arg" + index);
                }
            }

            CodeBlock functionInvokeCode = CodeBlock.of("(($T) obj).$L($L)",
                    MyInterface, methodName, CodeBlock.join(functionInvokeArgs, ", "));
            CodeBlock proxyMethodInvokeCodes;
            if (!TypeName.VOID.equals(returnType)) {
                functionInvokeCode = CodeBlock.builder()
                        .addStatement("return ($T) $L", returnType, functionInvokeCode)
                        .build();
                proxyMethodInvokeCodes = CodeBlock.of("return ($T) transact(0, $L$L)",
                        returnType, functionIdName, proxyMethodInvokeArgCode.build());
            } else {
                functionInvokeCode = CodeBlock.builder()
                        .addStatement(functionInvokeCode)
                        .addStatement("return null")
                        .build();
                proxyMethodInvokeCodes = CodeBlock.of("transact($T.MIN_TRANSACTION_ID, $L$L)",
                        t.IBinder, functionIdName, proxyMethodInvokeArgCode.build());
            }

            TypeSpec MyFunction = TypeSpec.anonymousClassBuilder("")
                    .addSuperinterface(t.Function)
                    .addMethod(MethodSpec.methodBuilder("invoke")
                            .addAnnotation(t.Override)
                            .addModifiers(Modifier.PUBLIC)
                            .addParameter(TypeName.OBJECT, "obj")
                            .addParameter(ArrayTypeName.of(TypeName.OBJECT), "args")
                            .returns(TypeName.OBJECT)
                            .addException(t.Throwable)
                            .addCode(functionInvokeCode)
                            .build())
                    .build();

            binderFunctionCode.addStatement("register($L, $L)", functionIdName, MyFunction);

            factoryFields.add(FieldSpec.builder(t.String, functionIdName)
                    .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                    .initializer(CodeBlock.builder()
                            .add("$S", getFunctionId(methodMember))
                            .build())
                    .build());

            proxyMethods.add(MethodSpec.methodBuilder(methodName)
                    .addModifiers(Modifier.PUBLIC)
                    .addAnnotation(t.Override)
                    .addParameters(proxyMethodParams)
                    .returns(returnType)
                    .addStatement(proxyMethodInvokeCodes)
                    .build());
            methodCount++;
        }

        ClassName MyFactory = ClassName.get(packageName, getClassName(element) + "Factory");
        ClassName MyBinder = MyFactory.nestedClass("MyBinder");
        ClassName MyProxy = MyFactory.nestedClass("MyProxy");

        TypeSpec MyFactorySpec = TypeSpec.classBuilder(MyFactory)
                .addModifiers(Modifier.FINAL)
                .addSuperinterface(t.OkBinderFactory)
                .addMethod(MethodSpec.methodBuilder("newBinder")
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(t.Class, "serviceClass")
                        .addParameter(TypeName.OBJECT, "remoteObject")
                        .returns(t.Binder)
                        .addAnnotation(t.Override)
                        .addStatement("return new $T(serviceClass, remoteObject)", MyBinder)
                        .build())
                .addMethod(MethodSpec.methodBuilder("newProxy")
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(t.Class, "serviceClass")
                        .addParameter(t.IBinder, "binder")
                        .returns(TypeName.OBJECT)
                        .addAnnotation(t.Override)
                        .addStatement("return new $T(serviceClass, binder)", MyProxy)
                        .build())
                .addFields(factoryFields)
                .addType(TypeSpec.classBuilder(MyProxy)
                        .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                        .superclass(t.BaseProxy)
                        .addSuperinterface(MyInterface)
                        .addMethod(MethodSpec.constructorBuilder()
                                .addParameter(ParameterSpec.builder(t.Class, "serviceClass").build())
                                .addParameter(ParameterSpec.builder(t.IBinder, "binder").build())
                                .addStatement("super(serviceClass, binder)")
                                .build())
                        .addMethods(proxyMethods)
                        .build())
                .addType(TypeSpec.classBuilder(MyBinder)
                        .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                        .superclass(t.BaseBinder)
                        .addMethod(MethodSpec.constructorBuilder()
                                .addParameter(ParameterSpec.builder(t.Class, "serviceClass").build())
                                .addParameter(ParameterSpec.builder(ClassName.OBJECT, "obj").build())
                                .addStatement("super(serviceClass, obj)")
                                .addCode(binderFunctionCode.build())
                                .build())
                        .build())
                .build();

        try {
            JavaFile.builder(packageName, MyFactorySpec)
                    .indent("    ")
                    .build()
                    .writeTo(filer);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private String getClassName(TypeElement element) {
        String fileName = elementUtils.getBinaryName(element).toString();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    private boolean shouldSkipMethod(Element member) {
        if (!(member instanceof ExecutableElement)) {
            return true;
        }
        for (Modifier modifier : member.getModifiers()) {
            if (modifier == Modifier.FINAL || modifier == Modifier.STATIC) {
                return true;
            }
        }
        if (!IS_SHOULD_SKIP_OBJECT_METHODS) {
            return false;
        }
        ExecutableElement methodMember = (ExecutableElement) member;
        String methodName = methodMember.getSimpleName().toString();
        List<? extends VariableElement> parameters = methodMember.getParameters();
        if (methodName.equals("toString") && parameters.isEmpty()) {
            return true;
        }
        if (methodName.equals("hashCode") && parameters.isEmpty()) {
            return true;
        }
        if (methodName.equals("equals") && parameters.size() == 1) {
            return ClassName.get(parameters.get(0).asType()).equals(TypeName.OBJECT);
        }
        return false;
    }

    private String getFunctionId(ExecutableElement method) {
        StringBuilder functionId = new StringBuilder(method.getSimpleName());
        StringBuilder params = new StringBuilder();
        boolean isFirst = true;
        for (VariableElement parameter : method.getParameters()) {
            TypeName paramType = TypeName.get(parameter.asType());
            params.append(isFirst ? "" : ",").append(paramType);
            isFirst = false;
        }
        if (params.length() <= 24) {
            return functionId.append("(").append(params).append(")").toString();
        }
        try {
            byte[] bytes = params.toString().getBytes();
            byte[] md5 = MessageDigest.getInstance("SHA-512").digest(bytes);
            String base64 = new String(Base64.getEncoder().encode(md5));
            return functionId.append("(").append(base64).append(")").toString();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
