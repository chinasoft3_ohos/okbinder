package cn.thens.okbinder2;

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import java.util.Collections;
import java.util.Set;

@AutoService(Processor.class)
public class OkbinderProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {
        Elements elementUtils = processingEnv.getElementUtils();
        Set<? extends Element> elements = env.getElementsAnnotatedWith(AIDL.class);
        RelatedTypes tt = new RelatedTypes();
        FactoryGenerator generator = new FactoryGenerator(tt, elementUtils, processingEnv.getFiler());
        for (Element element : elements) {
            TypeElement typeElement = (TypeElement) element;
            generator.generate(typeElement);
        }
        return false;
    }

    // 重写方法方式
    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(AIDL.class.getCanonicalName());
    }
}
