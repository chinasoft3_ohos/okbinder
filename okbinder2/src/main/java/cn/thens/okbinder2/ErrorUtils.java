package cn.thens.okbinder2;

final class ErrorUtils {
    /**
     * 错误
     *
     * @param error 数据
     * @return 错误
     */
    public static RuntimeException wrap(Throwable error) {
        if (error instanceof RuntimeException) {
            return (RuntimeException) error;
        }
        return new ErrorWrapper(error);
    }

    /**
     * 异常
     *
     * @param error 错误
     * @return 错误
     */
    public static Throwable unwrap(Throwable error) {
        if (error instanceof ErrorWrapper) {
            return error.getCause();
        }
        return error;
    }

    private static final class ErrorWrapper extends RuntimeException {
        ErrorWrapper(Throwable cause) {
            super(cause);
        }
    }
}
