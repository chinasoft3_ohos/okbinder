package cn.thens.okbinder2;

final class ObjectFunctions {
    private static final String TO_STRING = "toString()";
    private static final String HASH_CODE = "hashCode()";
    private static final String EQUALS = "equals(java.lang.Object)";

    private static final OkBinderFactory.Function TOSTRING = new OkBinderFactory.Function() {
        @Override
        public Object invoke(Object obj, Object[] args) {
            return obj.toString();
        }
    };

    private static final OkBinderFactory.Function HASHCODE = new OkBinderFactory.Function() {
        @Override
        public Object invoke(Object obj, Object[] args) {
            return obj.hashCode();
        }
    };

    private static final OkBinderFactory.Function IS_EQUALS = new OkBinderFactory.Function() {
        @Override
        public Object invoke(Object obj, Object[] args) {
            return obj.equals(args[0]);
        }
    };

    /**
     * 初始
     *
     * @param binder 数值
     */
    public static void inject(OkBinderFactory.BaseBinder binder) {
        binder.register(TO_STRING, TOSTRING);
        binder.register(HASH_CODE, HASHCODE);
        binder.register(EQUALS, IS_EQUALS);
    }

    /**
     * 字符
     *
     * @param proxy 属性
     * @return 是否相等
     */
    public static String toString(OkBinderFactory.BaseProxy proxy) {
        return (String) proxy.transact(0, TO_STRING);
    }

    /**
     * 比较
     *
     * @param proxy 属性
     * @return 是否相等
     */
    public static int hashCode(OkBinderFactory.BaseProxy proxy) {
        return (int) proxy.transact(0, HASH_CODE);
    }

    /**
     * 比较
     *
     * @param proxy 属性
     * @param obj 任意属性
     * @return 是否相等
     */
    public static boolean equals(OkBinderFactory.BaseProxy proxy, Object obj) {
        return (boolean) proxy.transact(0, EQUALS, obj);
    }
}
