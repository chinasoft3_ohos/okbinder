package cn.thens.okbinder2;

import ohos.rpc.*;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public interface OkBinderFactory {
    /**
     * 创建一个binder
     *
     * @param serviceClass 反射获取的类
     * @param remoteObject object类型
     * @return 类
     */
    RemoteObject newBinder(Class<?> serviceClass, Object remoteObject);

    /**
     * 获取一个代理
     *
     * @param serviceClass 服务类
     * @param binder 服务
     * @return 代理
     */
    Object newProxy(Class<?> serviceClass, IRemoteObject binder);

    interface Function {
        Object invoke(Object obj, Object[] args) throws Throwable;
    }

    class BaseBinder extends RemoteObject implements IRemoteBroker {
        protected final Object remoteObject;
        protected final String descriptor;
        protected final ClassLoader classLoader;
        private final Map<String, Function> functions = new HashMap<>();

        public BaseBinder(Class<?> serviceClass, final Object remoteObject) {
            super("descriptor");
            this.remoteObject = remoteObject;
            this.descriptor = serviceClass.getName();
            this.classLoader = remoteObject.getClass().getClassLoader();
            attachLocalInterface(this, descriptor);
            ObjectFunctions.inject(this);
        }

        protected void register(String methodId, Function func) {
            functions.put(methodId, func);
        }

        public Object getRemoteObject() {
            return remoteObject;
        }

        @Override
        public IRemoteObject asObject() {
            return this;
        }

        @Override
        public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply,
                                       MessageOption option) throws RemoteException {
            if (code == IRemoteObject.MIN_TRANSACTION_ID) {
                try {
                    data.readInterfaceToken();
                    int magicNumber = data.readInt();
                    if (magicNumber != OkBinderVersion.MAGIC_NUMBER) {
                        throw new IllegalArgumentException("Mismatched magic number " + magicNumber);
                    }
                    int version = data.readInt();
                    if (!OkBinderVersion.isSupported(version)) {
                        throw new IllegalArgumentException("Unsupported version " + version
                                + ", the min supported is " + OkBinderVersion.minSupported());
                    }
                    String functionId = data.readString();
                    int argCount = data.readInt();
                    Object[] args = new Object[argCount];
                    for (int i = 0; i < argCount; i++) {
                        args[i] = OkBinderParcel.read(data, classLoader);
                    }
                    Function function = functions.get(functionId);
                    if (function == null) {
                        throw new NoSuchElementException("Unregistered function " + functionId);
                    }
                    Object result = function.invoke(remoteObject, args);
                    if (reply != null) {
                        reply.writeNoException();
                        if (result != null) {
                            reply.writeInt(1);
                            OkBinderParcel.write(reply, result);
                        } else {
                            reply.writeInt(0);
                        }
                    }
                    return true;
                } catch (Throwable e) {
                    Throwable cause = ErrorUtils.unwrap(e);
                    LogUtile.e("Binder call failed" + cause);
                    RemoteException remoteException = new RemoteException();
                    remoteException.initCause(cause);
                    throw remoteException;
                }
            }
            return super.onRemoteRequest(code, data, reply, option);
        }
    }

    class BaseProxy {
        protected final IRemoteObject binder;
        protected final ClassLoader classLoader;
        protected final String descriptor;

        public BaseProxy(Class<?> serviceClass, IRemoteObject binder) {
            this.binder = binder;
            this.classLoader = serviceClass.getClassLoader();
            this.descriptor = serviceClass.getName();
        }

        protected Object transact(int flags, String methodId, Object... args) {
            if (binder.isObjectDead()) {
                throw new IllegalStateException("binder has died");
            }
            MessageParcel data = MessageParcel.obtain();
            MessageParcel reply = MessageParcel.obtain();
            Object result = null;
            try {
                data.writeInterfaceToken(descriptor);
                data.writeInt(OkBinderVersion.MAGIC_NUMBER);
                data.writeInt(OkBinderVersion.current());
                data.writeString(methodId);
                if (args != null) {
                    data.writeInt(args.length);
                    for (Object arg : args) {
                        OkBinderParcel.write(data, arg);
                    }
                } else {
                    data.writeInt(0);
                }
                MessageOption option = new MessageOption();
                option.setFlags(flags);
                binder.sendRequest(IRemoteObject.MIN_TRANSACTION_ID,data,reply,option);
                reply.readException();
                if (reply.readInt() != 0) {
                    result = OkBinderParcel.read(reply, classLoader);
                }
            } catch (Throwable e) {
                throw ErrorUtils.wrap(e);
            } finally {
                reply.reclaim();
                data.reclaim();
            }
            return result;
        }

        @Override
        public int hashCode() {
            return ObjectFunctions.hashCode(this);
        }

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object obj) {
            return ObjectFunctions.equals(this, obj);
        }

        @SuppressWarnings("NullableProblems")
        @Override
        public String toString() {
            return ObjectFunctions.toString(this);
        }
    }
}
