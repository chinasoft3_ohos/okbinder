package cn.thens.okbinder2;

import ohos.rpc.IRemoteObject;
import ohos.rpc.MessageParcel;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"rawtypes", "unchecked"})
public final class OkBinderParcel {
    private static final int VAL_DEFAULT = 1;
    private static final int VAL_LIST = 2;
    private static final int VAL_SPARSE_ARRAY = 3;
    private static final int VAL_MAP = 4;
    private static final int VAL_OK_BINDER = 5;
    private static final int VAL_ARRAY = 6;

    /**
     * 写入对应的数据
     *
     * @param parcel 写入
     * @param vv object数据类型
     */
    public static void write(MessageParcel parcel,Object vv) {
        if (vv == null) {
            parcel.writeInt(VAL_DEFAULT);
            parcel.writeValue(null);
        } else if (vv instanceof List) {
            parcel.writeInt(VAL_LIST);
            List val = (List) vv;
            parcel.writeInt(val.size());
            for (Object ob : val) {
                write(parcel, ob);
            }
        }  else if (vv instanceof Map) {
            parcel.writeInt(VAL_MAP);
            Map val = (Map) vv;
            Set<Map.Entry<Object, Object>> entries = val.entrySet();
            parcel.writeInt(entries.size());
            for (Map.Entry<Object, Object> key : entries) {
                write(parcel, key.getKey());
                write(parcel, key.getValue());
            }
        } else if (vv.getClass().isArray()) {
            parcel.writeInt(VAL_ARRAY);
            Class<?> componentType = vv.getClass().getComponentType();
            if (componentType.isPrimitive()) {
                parcel.writeString("");
                parcel.writeValue(vv);
                return;
            }
            parcel.writeString(componentType.getName());
            Object[] val = (Object[]) vv;
            parcel.writeInt(val.length);
            for (Object ob : val) {
                write(parcel, ob);
            }
        } else {
            Class<?> okBinderInterface = OkBinder.getOkBinderInterface(vv);
            if (okBinderInterface != null) {
                parcel.writeInt(VAL_OK_BINDER);
                parcel.writeString(okBinderInterface.getName());
                parcel.writeValue(OkBinder.create((Class<Object>) okBinderInterface, vv));
                return;
            }
            parcel.writeInt(VAL_DEFAULT);
            parcel.writeValue(vv);
        }
    }

    /**
     * 读取
     *
     * @param parcel 序列化
     * @param loader 类
     * @return 读取得到的数据
     * @throws ClassNotFoundException 类异常
     */
    public static Object read(MessageParcel parcel, ClassLoader loader) throws ClassNotFoundException {
        int type = parcel.readInt();
        switch (type) {
            case VAL_LIST: {
                List outVal = new ArrayList();
                int size = parcel.readInt();
                for (int index = 0; index < size; index++) {
                    outVal.add(read(parcel, loader));
                }
                return outVal;
            }
            case VAL_SPARSE_ARRAY: {
                HashMap outVal = new HashMap<>();
                int size = parcel.readInt();
                for (int index = 0; index < size; index++) {
                    int key = parcel.readInt();
                    Object value = read(parcel, loader);
                    outVal.put(key, value);
                }
                return outVal;
            }
            case VAL_MAP: {
                Map outVal = new HashMap();
                int size = parcel.readInt();
                for (int index = 0; index < size; index++) {
                    Object key = read(parcel, loader);
                    Object value = read(parcel, loader);
                    outVal.put(key, value);
                }
                return outVal;
            }
            case VAL_OK_BINDER: {
                Class<?> serviceClass = loader.loadClass(parcel.readString());
                parcel.writeValue(loader);
                IRemoteObject binder = (IRemoteObject) parcel.readValue();
                return OkBinder.proxy(serviceClass, binder);
            }
            case VAL_ARRAY: {
                String componentName = parcel.readString();
                boolean isPrimitiveComponent = "".equals(componentName);
                if (isPrimitiveComponent) {
                    return parcel.readValue();
                }
                Class<?> componentClass = loader.loadClass(componentName);
                int size = parcel.readInt();
                Object[] outputVal = (Object[]) Array.newInstance(componentClass, size);
                for (int index = 0; index < size; index++) {
                    outputVal[index] = read(parcel, loader);
                }
                return outputVal;
            }
            default:
                return parcel.readValue();
        }
    }
}
