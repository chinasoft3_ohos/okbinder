package cn.thens.okbinder2;

public final class OkBinderVersion {
    /**
     * 魔鬼数字
     */
    public static final int MAGIC_NUMBER = -1588420922;
    /**
     * 魔鬼数字
     */
    public static final int V20 = 20;

    /**
     * 当前
     *
     * @return int类型
     */
    public static int current() {
        return V20;
    }

    /**
     * 最小
     *
     * @return int类型
     */
    public static int minSupported() {
        return V20;
    }

    /**
     * 是否传入值大于最小值
     *
     * @param version 版本
     * @return true 是，反之
     */
    public static boolean isSupported(int version) {
        return version >= minSupported();
    }
}
