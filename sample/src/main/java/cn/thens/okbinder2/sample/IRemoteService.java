package cn.thens.okbinder2.sample;

import cn.thens.okbinder2.AIDL;

import java.util.List;
import java.util.Map;

/**
 * @author 7hens
 */
@SuppressWarnings("UnusedReturnValue")
@AIDL
public interface IRemoteService {
    /**
     * 字符串
     *
     * @return 返回字符串
     */
    String test();

    /**
     * 返回布尔和字符串
     *
     * @param isBoolean isBoolean
     * @param aParcelable 字符串
     */
    void testError(Boolean isBoolean, String aParcelable);

    /**
     * 字符串
     *
     * @param callback 字符串
     */
    void testCallback(String callback);

    /**
     * list类型
     *
     * @param list 集合
     */
    void testList(List<String> list);

    /**
     * map类型
     *
     * @param map map类型
     */
    void testSparseArray(Map<String, String> map);

    /**
     * 数组
     *
     * @param array 字符串数组
     */
    void testObjectArray(String[] array);

    /**
     * map类型
     *
     * @param map map
     */
    void testMap(Map<String, String> map);

    /**
     * 数组
     *
     * @param array 字符串数组
     */
    void testAidlArray(String[] array);

    /**
     * 数组
     *
     * @param array int数组
     */
    void testPrimitiveArray(int[] array);

    /**
     * int数组
     *
     * @param array 数组
     */
    void testPrimitiveArray2(int[] array);
}
