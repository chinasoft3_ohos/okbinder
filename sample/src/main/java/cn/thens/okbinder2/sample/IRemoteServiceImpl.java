package cn.thens.okbinder2.sample;

import cn.thens.okbinder2.LogUtile;
import ohos.hiviewdfx.HiLog;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author 7hens
 */
public final class IRemoteServiceImpl implements IRemoteService {
    private static final String TAG = "@OkBinder";
    private final String tag;

    /**
     * 构造
     *
     * @param tag 文本
     */
    public IRemoteServiceImpl(String tag) {
        this.tag = tag;
    }

    private String log(String text) {
        String msg = toString() + "." + text;
        LogUtile.i(TAG + " " + msg);
        return msg;
    }

    @Override
    public String test() {
        return log("test()");
    }

    @Override
    public void testCallback(String callback) {
        log("testCallback(" + callback + ") ");
        log("test()");
    }

    @Override
    public void testList(List<String> list) {
        log("testList(" + list + ") ");
    }

    @Override
    public void testSparseArray(Map<String, String> map) {
        log("testSparseArray(" + map + ") ");
    }

    @Override
    public void testObjectArray(String[] array) {
        log("testObjectArray(" + Arrays.toString(array) + ") ");
    }

    @Override
    public void testError(Boolean isBoolean, String aParcelable) {
        try {
            log("testError(" + isBoolean + ", " + aParcelable + ")");
            throw new NullPointerException();
        } catch (Exception e) {
            LogUtile.e("MainAbility: testError => \n" + HiLog.getStackTrace(e));
        }
    }

    @Override
    public void testMap(Map<String, String> map) {
        log("testMap(" + map + ") ");
    }

    @Override
    public void testAidlArray(String[] array) {
        log("testAidlArray(" + Arrays.toString(array) + ") ");
    }

    @Override
    public void testPrimitiveArray(int[] array) {
        log("testPrimitiveArray(" + Arrays.toString(array) + ") ");
    }

    @Override
    public void testPrimitiveArray2(int[] array) {
        log("testPrimitiveArray2(" + Arrays.toString(array) + ") ");
    }

    @Override
    public String toString() {
        return "IParcelableImpl_" + tag;
    }
}
