package cn.thens.okbinder2.sample;

import cn.thens.okbinder2.OkBinder;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.rpc.IRemoteObject;

/**
 * @author 7hens
 */
public class LocalService extends Ability {
    @Override
    protected IRemoteObject onConnect(Intent intent) {
        return OkBinder.create(new IRemoteServiceImpl("LocalService"));
    }
}
