package cn.thens.okbinder2.sample.slice;

import cn.thens.okbinder2.*;
import cn.thens.okbinder2.sample.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.ElementName;
import ohos.rpc.IRemoteObject;

import java.util.*;

public class MainAbilitySlice extends AbilitySlice {
    private Map<String, String> map = new HashMap<>();
    private Map<String, String> sparse = new HashMap<>();
    private String replace;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ElementName elementName = ElementName.unflattenFromString(" /a.b/.c");
        String uri = elementName.getURI();
        replace = uri.replace(" /", "");
        IRemoteService callback1 = new IRemoteServiceImpl("MainAbility1");
        IRemoteService callback2 = new IRemoteServiceImpl("MainAbility2");
        map.put("mapKey", callback1.toString());
        sparse.put("1", callback1.toString());
        sparse.put("2", callback2.toString());
        int[] intArray = {1, 2};
        String[] mIdlArray = {callback1.toString(), callback2.toString()};
        findComponentById(ResourceTable.Id_vTestRemoteService).setClickedListener(component -> {
            new ToastDialog(MainAbilitySlice.this).setText("please check the log").show();
            Intent intents = new Intent();
            startService(intents, RemoteService.class.getName(), MainAbilitySlice.this);
            MainAbilitySlice.this.connectAbility(intents, new IAbilityConnection() {
                @Override
                public void onAbilityConnectDone(ElementName elementName, IRemoteObject remote, int i) {
                    IRemoteService proxy = OkBinder.proxy(IRemoteService.class, remote);
                    proxy.testError(false, replace);
                    proxy.testCallback(callback1.toString());
                    proxy.testList(Arrays.asList(callback1.toString(), callback2.toString()));
                    proxy.testSparseArray(map);
                    proxy.testMap(map);
                    proxy.testAidlArray(mIdlArray);
                    proxy.testObjectArray(mIdlArray);
                    proxy.testPrimitiveArray(intArray);
                    proxy.testPrimitiveArray2(intArray);
                }

                @Override
                public void onAbilityDisconnectDone(ElementName elementName, int i) {
                }
            });
        });
        findComponentById(ResourceTable.Id_vTestLocalService).setClickedListener(component -> {
            new ToastDialog(MainAbilitySlice.this).setText("please check the log").show();
            Intent intents = new Intent();
            startService(intents, LocalService.class.getName(), MainAbilitySlice.this);
            MainAbilitySlice.this.connectAbility(intents, new IAbilityConnection() {
                @Override
                public void onAbilityConnectDone(ElementName elementName, IRemoteObject remote, int i) {
                    IRemoteService proxy = OkBinder.proxy(IRemoteService.class, remote);
                    proxy.testError(false, replace);
                    proxy.testCallback(callback1.toString());
                    proxy.testList(Arrays.asList(callback1.toString(), callback2.toString()));
                    proxy.testSparseArray(map);
                    proxy.testMap(map);
                    proxy.testAidlArray(mIdlArray);
                    proxy.testObjectArray(mIdlArray);
                    proxy.testPrimitiveArray(intArray);
                    proxy.testPrimitiveArray2(intArray);
                }

                @Override
                public void onAbilityDisconnectDone(ElementName elementName, int i) {
                }
            });
        });
    }

    private void startService(Intent intents, String name, MainAbilitySlice mainAbilitySlice) {
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(mainAbilitySlice.getAbilityPackageContext().getBundleName())
                .withAbilityName(name)
                .build();
        intents.setOperation(operation);
        mainAbilitySlice.startAbility(intents, 0);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
