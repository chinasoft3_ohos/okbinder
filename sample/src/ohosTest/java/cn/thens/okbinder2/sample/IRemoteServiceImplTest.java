package cn.thens.okbinder2.sample;

import cn.thens.okbinder2.OkBinder;
import ohos.rpc.*;
import org.junit.Test;

import java.io.FileDescriptor;
import static org.junit.Assert.*;

public class IRemoteServiceImplTest {

    @Test
    public void test1() {
        String test = "test";
        assertEquals(test,"test");
    }

    @Test
    public void testPid() {
        RemoteObject remoteObject = OkBinder.create(new IRemoteServiceImpl("test"));
        assertEquals(remoteObject.isObjectDead(),false);
    }

    @Test
    public void ObjectDead() {
        IRemoteObject remoteObject = new IRemoteObject() {
            @Override
            public IRemoteBroker queryLocalInterface(String s) {
                return null;
            }

            @Override
            public boolean sendRequest(int i, MessageParcel messageParcel, MessageParcel messageParcel1, MessageOption messageOption) throws RemoteException {
                return false;
            }

            @Override
            public boolean addDeathRecipient(DeathRecipient deathRecipient, int i) {
                return false;
            }

            @Override
            public boolean removeDeathRecipient(DeathRecipient deathRecipient, int i) {
                return false;
            }

            @Override
            public String getInterfaceDescriptor() {
                return null;
            }

            @Override
            public void dump(FileDescriptor fileDescriptor, String[] strings) throws RemoteException {

            }

            @Override
            public void slowPathDump(FileDescriptor fileDescriptor, String[] strings) throws RemoteException {

            }

            @Override
            public boolean isObjectDead() {
                return true;
            }
        };
        OkBinder.proxy(IRemoteService.class, remoteObject);
        boolean objectDead = remoteObject.isObjectDead();
        assertEquals(objectDead,true);
    }
}